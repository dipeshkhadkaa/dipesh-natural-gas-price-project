# Dipesh Natural Gas Price Project

Natural Gas Price Project for Henry gas

## Library & Modules Used:
* Beautiful Soup
* Pandas
* datetime

Exception Handling:
* urllib.error
* urllib.request

[Henry Hub Natural Gas Project](https://www.eia.gov/dnav/ng/hist/rngwhhdD.htm) have the columns of data 
containing the date and the price of Gas on daily, weekly and monthly basis.
Above code scrapt the data on basis of daily basis and export it into .csv file. 

## How to run

* Download the file gas_price_project.py
* Go into your teminal and check if there is python
* Type python gas_price_project.py 
* And you see new file with .csv format i.e. Gas Price.csv

**Note: Above project is based on python3. You will get error if you use any lower version**

## How does it work

* First part of the code imports the required modules and the libraries.
* Second part of the code is responsible for exception handling if the location of website scrapred is not found.
* Third part searches the table (it is the only one that has the attribute summary) and loops over each row (tr).
* Fourth part gets from the Week column (td class B6) the first part before the " to " and convert it to an datetime.
* For each cell (td class B3) it get the price (or empty string), set the date and increments the date.
* Using Pandas it concat the two column and export it into .csv file format which is done at the last part of the code. 

